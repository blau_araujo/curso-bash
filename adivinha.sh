!/usr/bin/env bash

# Mensagens do jogo ---------------------------------------

msg_chute='Chute um número de 1 a 100: '
msg_acerto='Parabéns! Você acertou!'
msg_tentativas='Só precisou de %d tentativas...'
msg_sair='Até mais!'

# Mensagens de erro ---------------------------------------

erro[1]='Eu disse um número!'
erro[2]='Eu disse de 1 a 100!'
erro[3]='Grande demais'
erro[4]='Pequeno demais'

# Funções -------------------------------------------------

# Pedir palpite
_pede_palpite() {
    local palpite
    read -p "$msg_chute" palpite
    _testa_palpite $palpite
}

# Testar palpite...
_testa_palpite() {
    [[ $1 -eq $secreto ]]        && return 0
    [[ ! $1 =~ ^[0-9]+$ ]]       && return 1
    [[ $1 -lt 1 || $1 -gt 100 ]] && return 2
    [[ $1 -gt $secreto ]]        && return 3
    [[ $1 -lt $secreto ]]        && return 4
}

# Sair...
_sair() {
    echo $msg_sair$'\n'
    exit
}


# Principal -----------------------------------------------

# Número secreto...
secreto=$(( ($RANDOM % 100) + 1 ))

# Iniciar contador...
((tentativas=1))

clear

# Loop infinito...
while :; do
    # Pedir palpite...
    if _pede_palpite; then
        echo $'\n'$msg_acerto
        printf "$msg_tentativas\n\n" $tentativas
        _sair
     else
        echo ${erro[$?]}
        ((tentativas++))
    fi
done

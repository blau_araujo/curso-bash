#!/usr/bin/env bash

clear

echo "COMPARANDO PID'S DAS SESSÕES DO BASH NO SCRIPT E NO TERMINAL"
echo "------------------------------------------------------------"
echo "PID da sessão do Bash no terminal : $PPID"
echo "PID da sessão do Bash neste script: $$"
echo "------------------------------------------------------------"

ps -p $$,$PPID -o pid=,user=,tty=,args=

exit
#!/usr/bin/env bash

# Quando vamos testar possíveis erros, temos que montar as expressões
# de forma que elas representem esses erros de forma *positiva*! Ou seja,
# é bem melhor quando o status de saída do teste é "sucesso" (0).
#
# Pense sempre na pergunta: é um erro?
# E execute algo se a resposta for "sim".
#
# Exemplos:
#
# Erro: o número de parâmetros é diferente de 1?
# [[ $# -ne 1 ]] && echo "Número incorreto de parâmetros!" && exit
#
# Erro: o parâmetro informado é um diretório?
# [[ -d $1 ]] && echo "Isso é um diretório!" && exit
#
# Erro: o arquivo já existe?
# [[ -f $1 ]] && echo "Arquivo já existe!" && exit


# Aqui, estamos utilizando o operador "ou" (||) para fazer os três testes
# de uma só vez. Caso qualquer um deles aconteça, o teste inteiro vai
# sair com status "0" (sucesso).

[[ $# -ne 1 || -d $1 || -f $1  ]] && exit 1

# Aqui estamos testando se o usuário tem um editor padrão definido
# no arquivo "~/.selected_editor". De novo, queremos um status de
# sucesso na saída do teste, por isso estamos negando (!) a avaliação
# da expressão.

[[ ! -f $HOME/.selected_editor ]] && select-editor
source $HOME/.selected_editor

# Feitos todos os testes, não havendo erros, o novo script é criado...

echo '#!/usr/bin/env bash' > $1

chmod +x $1

$SELECTED_EDITOR $1

exit

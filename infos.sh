#!/usr/bin/env bash

clear

echo "INFORMAÇÕES SUPER IMPORTANTES"
echo "-----------------------------------------"
echo -n "Usuário : "
whoami
echo -n "Hostname: "
hostname
echo -n "Uptime  : "
uptime -p
echo -n "Kernel  : "
uname -rms
echo "-----------------------------------------"

